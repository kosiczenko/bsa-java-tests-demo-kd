package com.example.demo.service;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ToDoServiceTest {

    ToDoRepository toDoRepository;
    ToDoService toDoService;


    @BeforeEach
    void setUp() {
        toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenUpsertIncorrectId_thenThrowToDoNotFoundException() {
        var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = -1L;
        toDoSaveRequest.text = "Incorrect ID Not found";
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoSaveRequest));
    }

    @Test
    void whenCompleteToDo_thenSaveCalled() throws ToDoNotFoundException {
        ToDoEntity toDoEntity = new ToDoEntity(1L, "I do what I want");
        when(toDoRepository.findById(1L)).thenReturn(Optional.of(toDoEntity));
        when(toDoRepository.save(toDoEntity)).thenReturn(toDoEntity);

        toDoService.completeToDo(toDoEntity.getId());
        verify(toDoRepository, times(1)).save(toDoEntity);
    }

    @Test
    void whenGetOneById_thanReturnEntity() throws ToDoNotFoundException {
        ToDoEntity toDoEntity = new ToDoEntity(1L, "Do what you want");
        when(toDoRepository.findById(1L)).thenReturn(Optional.of(toDoEntity));

        var result = toDoService.getOne(toDoEntity.getId());
        assertEquals(result.id, toDoEntity.getId());
        assertEquals(result.text, toDoEntity.getText());
    }

    @Test
    void whenCallCompleteToDo_thenReturnWithLaterCompleteTime() throws ToDoNotFoundException {
        var testStartTime = ZonedDateTime.now();
        ToDoEntity toDoEntity = new ToDoEntity(1L, "Do what you want");
        when(toDoRepository.findById(1L)).thenReturn(Optional.of(toDoEntity));
        when(toDoRepository.save(toDoEntity)).thenReturn(toDoEntity);

        var result = toDoService.completeToDo(toDoEntity.getId());

        assertTrue(testStartTime.isBefore(result.completedAt));
    }

}