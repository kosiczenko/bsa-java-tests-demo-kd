package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    private final long GOOD_ID = 1L;
    private final long BAD_ID = -1L;

    @Test
    void whenDeleteOne_thanGetValidResponse() throws Exception {
        doNothing().when(toDoService).deleteOne(GOOD_ID);

        this.mockMvc
                .perform(delete("/todos/" + GOOD_ID))
                .andExpect(status().is(200));
    }

    @Test
    void whenDeleteInvalidId_thanExceptionButStatusOk() {
        doThrow(IllegalArgumentException.class)
                .when(toDoService)
                .deleteOne(BAD_ID);

        assertThrows(NestedServletException.class, () ->
                mockMvc.perform(delete("/todos/" + BAD_ID))
                        .andExpect(status().isOk()));
    }

    @Test
    void whenGetInvalidId_thanExceptionButStatusOk() throws ToDoNotFoundException {
        doThrow(IllegalArgumentException.class)
                .when(toDoService)
                .getOne(BAD_ID);

        assertThrows(NestedServletException.class, () ->
                mockMvc.perform(get("/todos/" + BAD_ID))
                        .andExpect(status().isOk()));
    }


}