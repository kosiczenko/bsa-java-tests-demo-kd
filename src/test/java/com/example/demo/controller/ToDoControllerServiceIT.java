package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ToDoRepository toDoRepository;

    private final String BAD_ID = "-1";
    private final long GOOD_ID = 1L;

    @Test
    void whenGetInvalidId_thenException() throws Exception {

        mockMvc
                .perform(get("/todos/" + BAD_ID))
                //.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string("Can not find todo with id " + BAD_ID));
    }

    @Test
    void whenIdIsPresent_thenReturnEntity() throws Exception {
        ToDoEntity toDoEntity = new ToDoEntity(GOOD_ID, "ToDoEntity");

        when(toDoRepository.findById(GOOD_ID)).thenReturn(Optional.of(toDoEntity));

        mockMvc
                .perform(get("/todos/" + GOOD_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("text").value(toDoEntity.getText()))
                .andExpect(jsonPath("id").isNumber())
                .andExpect(jsonPath("id").value(GOOD_ID));
    }

}